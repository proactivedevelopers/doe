<?php
//include_once("includes/functions.php");
$do = "default";
if (!isset($_COOKIE['password']))
    header("location:index.php");
if (isset($_GET['do']))
    $do = $_GET['do'];
else
    $do = "default";
include_once("includes/connect.php");
if ($do == "enter_rec" || $do == "edit_record" || $do == "select_marksheet" || $do == "view_ledger" || $do == "showind")
    if (!isset($_COOKIE['year']))
        header("location:first.php?do=enter_year");
    elseif (!isset($_COOKIE['school']))
        header("location:first.php?do=enter_sch");
if (isset($_COOKIE['year']))
    $yearr = $_COOKIE['year'];
if (isset($_COOKIE['school']))
    $schooll = $_COOKIE['school'];
$cur = mysql_result(mysql_query("select * from curriculum"), 0, 0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link href="style.css" rel="stylesheet" type="text/css"/>
    <link href="pics/icon.ico" rel="icon" type="image/x-icon"/>
    <link rel="shortcut icon" href="pics/icon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="autocomplete.css" type="text/css" media="screen">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Result Processing System, Hetauda Submetropolitan City</title>
</head>
<?php
if ($do == 'enter_rec')
echo "<body onLoad='main.sn.focus();'>";
elseif ($do == 'enter_school')
    echo "<body onload='sc_form.namee.focus();'>";
elseif ($do == 'enter_year')
    echo "<body onload='mero.year.focus();'>";
elseif ($do == 'enter_subject')
    echo "<body onload='sub_form.year.focus();'>";
elseif ($do == 'set_year_fmpm')
{
?>
<body onload="document.getElementById('yrr').focus();">
<?php }
elseif ($do == 'enter_sch')
    echo "<body onload='mero.school.focus();'>";
elseif ($do == 'dododo')
    echo "<body onload='mero.school.focus();'>";
elseif ($do == 'doitdo')
    echo "<body onload='mero.school.focus();'>";
elseif ($do == 'validate_edit')
    echo "<body onload='mero.pass.focus();'>";
elseif ($do == "enter_grace" || $do == "edit_grace")
    echo "<body onload='sc_form.year.focus();'>";
elseif ($do == 'default') {
    ?>
    <script src="jquery.js" type="text/javascript"></script>
    <script src="jquery.min.js" type="text/javascript"></script>
    <script src="dimensions.js" type="text/javascript"></script>
    <script src="autocomplete.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(function () {
            setAutoComplete("searchField", "results", "autocomplete.php?part=");
        });

    </script>
    <?php
    echo "<body>";
} else {
    ?>
    <script src="jquery.js" type="text/javascript"></script>
    <script src="jquery.min.js" type="text/javascript"></script>
    <script src="dimensions.js" type="text/javascript"></script>
    <script src="autocomplete.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(function () {
            setAutoComplete("searchField", "results", "autocomplete.php?part=");
        });

    </script>
    <?php
    echo "<body>";
}
?>
<div id="banner">
    <!--<a href="#"><img src="pics/homee.png" width="30" height="30" hspace="20" vspace="20"/></a>-->
    <!--    <a href=""><img src="pics/homee.png" width="30" height="30" hspace="20" vspace="20"/></a>-->
</div>
<div id="mainbody"> <!-- main body #######################################-->
    <!--<div id="menu">
    </div>-->
    <div id=nav>
        <a href="first.php">Home</a><a href="first.php?do=select_marksheet">Marksheet</a><a
                href="first.php?do=view_ledger">Ledger</a><a href="first.php?do=showind">Individual Entry</a><a
                href="first.php?do=enter_rec">Enter Record</a><a href="first.php?do=settings">Settings</a><a
                href="import_first.php">Import Record</a><a href="import_marks.php">Import Marks</a><a
                href="marksheet_auto1.php" target="_blank">Auto MK Sheet</a>
    </div>
    <div id="body">
        <?php
        if ($do == "enter_rec")
            include("enter_record.php");
        elseif ($do == "enter_year")
            include("enter_year.php");
        elseif ($do == "enter_year_flush")
            include("enter_year_flush.php");
        elseif ($do == "enter_school")
            include("enter_school.php");
        elseif ($do == "enter_sch_flush")
            include("enter_school_flush.php");
        elseif ($do == "delsuccess")
            include("delsuccess.php");
        elseif ($do == "enter_grace")
            include("enter_grace.php");
        elseif ($do == "enter_subject")
            include("enter_subject.php");
        elseif ($do == "enter_sch")
            include("next.php");
        elseif ($do == "enter_sch123")
            include("next123.php");
        elseif ($do == "view_mksheet")
            include("select_student.php");
        elseif ($do == "view_ledger")
            include("select_ledger.php");
        elseif ($do == "edit_record")
            include("edit_record.php");
        elseif ($do == "select_marksheet")
            include("select_marksheet.php");
        elseif ($do == "showedit")
            include("includes/show_edit_record.php");
        elseif ($do == "confirm")
            include("includes/confirm.php");
        elseif ($do == "set_fmpm")
            include("set_fmpm.php");
        elseif ($do == "settings")
            include("includes/setting.php");
        elseif ($do == "dododo")
            include("next1.php");
        elseif ($do == "change_school_for_marks")
            include("change_school_for_marks.php");
        elseif ($do == "doitdo")
            include("next11.php");
        elseif ($do == "doitdoo")
            include("next111.php");
        elseif ($do == "showind")
            include("show_indi_enter.php");
        elseif ($do == "indient")
            include("indi_enter.php");
        elseif ($do == "setting_fmpm")
            include("includes/setting_fmpm.php");
        elseif ($do == "detail_fmpm")
            include("includes/detail_fmpm.php");
        elseif ($do == "set_year_fmpm")
            include("set_year_fmpm.php");
        elseif ($do == "stdDtl")
            include("includes/studentDetail.php");
        elseif ($do == "shRes")
            include("includes/showSearchResult.php");
        elseif ($do == "report_overall") {
            include("report_overall.php");
            echo "<br><a href=report_overall.php><input type=button value='Preview Printable'></a>";
        } elseif ($do == "validate_edit")
            include("includes/validate_edit.php");
        elseif ($do == "default") {

            ?>

            <div style="float:right; padding:3px;">
                <form action="includes/searchStudent.php" method="post" name="form1">
                    <p id="auto">
                        <input id="searchField" placeholder="Search Student" name="first" type="text"
                               style="width:150px;"/>&nbsp;&nbsp;<input
                                type="submit" value="Search"/>
                    </p>

                </form>
            </div>
            <br clear="all">

            <center>
                <img src="pics/disp.png"/>
                <p style="text-align:center;">
                    <?php
                    if (isset($_COOKIE['school'])) {
                        include_once("includes/connect.php");
                        function schoolfromid($id)
                        {
                            $data = mysql_query("select name from schools where schoolcode='$id'");
                            $d = mysql_fetch_row($data);
                            return $d[0];
                        }

                        echo $schoolkocode = $_COOKIE['school'];
                        echo "<br>";
                        echo $schoolName =  schoolfromid($_COOKIE['school']);
                        ?>
                        <br>
                        <a href="includes/updategradesheet.php?school=<?php echo $schoolkocode;?>" style="text-decoration:none; color:#990000">    
                            Update GradeSheet OF <?php echo $schoolName; ?> Only
                        </a>
                        <?php 
                    }

                    ?>
                </p>
                <p>
                    <?php if (isset($_GET['sc']))
                        if ($_GET['sc'] == "yes")
                            echo "<font color=green>Database Successfully Updated</font>";
                    ?>

                    <?php if (isset($_GET['sc']))
                        if ($_GET['sc'] == "gradesheet")
                            echo "<font color=green>Gradesheet Updated Successfully</font>";
                    ?></p>
                <a href="first.php?do=doitdoo"
                   style="text-decoration:none; color:#990000; display: inline-block;background: brown; color: #fff; padding: 10px;">
                    Change School
                </a>
                <br>
                <br>
                <a href="includes/updateall.php" style="text-decoration:none; color:#990000">Update Database</a>
                <br> <br>
                <a href="includes/updategradesheet.php" style="text-decoration:none; color:#990000">Update Grade
                    Sheet</a>
                <br/><br/>
                <br/>
            </center>

            <?php
        }
        ?>
    </div>
    <br style="clear:both;"/>
    <div style="float:right; padding-right: 15px;">For Support: <a target="_blank" href="https://www.proactivedevelopers.com">ProActive Web Developers</a></div>
    <br/>
</div>           <!-- main body #######################################-->
</body>
</html>