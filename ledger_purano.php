<?php
include("includes/connect.php");
include("includes/functions.php");
if(!isset($_COOKIE['school']) && !isset($_COOKIE['year']))
header("location:index.php?do=enter_year");
$school=$_COOKIE['school'];
$year=$_COOKIE['year'];
$sturec=mysql_query("SELECT * FROM `main` where `year`=$year AND `school`='$school' order by sn");
$subjects=mysql_query("SELECT * from subject where `year`=$year");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DEO Makawanput, Result Processing System</title>
<style type="text/css">
<!--
*
{
margin:0px;
font-size:10pt;
}
h1
{
font-size:16pt;
}
h2
{
font-size:12pt;
}
h3
{
font-size:16pt;
}
.aa th, .aa td
{
border:#AAAAAA thin solid;
border-collapse:collapse;
}
.aa table, .aa td, .aa th, .aa tr
{
border:#AAAAAA thin solid;
border-collapse:collapse;
}
-->
</style>
</head>

<body>

<table cellspacing="0" cellpadding="2" width="1100" class="aa">
<tr>
<td colspan=33><H3><center>District Level Examination Board<br>Basic Level Final Examination</center><br><?php echo schoolfromid($school);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;School code: <?php echo $school;?></H3>
</td>
</tr>


<tr>
<td colspan=33 align=center>


<a href="first.php?do=doitdo" style="text-decoration:none;color:black">


<table border="1" cellspacing="0" cellpadding="5">
<tr><th>Distinction</th><th>1<sup>st</sup></th><th>2<sup>nd</sup></th><th>3<sup>rd</sup></th><th>Failed</th><th>Total</th><th>Pass %</th></tr>
<?php $rr=result_overall($school);?>
<tr>
<th><?php echo $rr[0];?></TH>
<th><?php echo $rr[1];?></TH>
<th><?php echo $rr[2];?></TH>
<th><?php echo $rr[3];?></TH>
<th><?php echo $rr[4];?></TH>
<th><?php echo $rr[5];?></TH>
<th>
<?php 
$p=$rr[5]-$rr[4];
$calc=$p/$rr[5]*100;
echo floor($calc);
?></th>

</TR>
</table>

</a>

</td>
</tr>




<tr>
<th rowspan="2">SN</th>
<th rowspan="2">Name</th>
<?php
$data=mysql_fetch_row($subjects);
$no=$data[2];
for($i=3;$i<=$no+2;$i++)
echo "<th colspan=3>".substr($data[$i],0,8)."</TH>";
?>
<th rowspan="2">Tot</th>
<th rowspan="2">%</th>
<th rowspan="2">Result</th>
<th rowspan="2">Rem</th>
</tr>
<tr>
<?php
for($i=1;$i<=$no;$i++)
echo "<td><font size=2>TH</font></td><td><font size=2>PR</font></td><td><font size=2>ToT</font></td>";
?>
</tr>
<?php
while($ddata=mysql_fetch_row($sturec))
{
echo "<tr>";
echo "<td>".$ddata[1]."</td>";
echo "<th align=left>".$ddata[2]."</th>";
$rowcount=6;
$tot=0;
$res="PASS";


for($i=1;$i<=$no;$i++)
{
$first=$ddata[$rowcount++];
$tot+=$first;
echo "<td>".$first."</td><td>";
$second=$ddata[$rowcount++];
$tot+=$second;
if($second==0)
echo "&nbsp;";
else
echo $second;
$tt=$first+$second;
echo "</td><td>".$tt."</td>";
if($tt<=32)
$res="FAIL";
}
echo "<td>".$tot."</td>";
echo "<td>".round((($tot/givetotal($year))*100),2)."</td>";
echo "<td>".$ddata[28]."</td>";
echo "<td>";
echo $ddata[29];
echo "</td>";
echo "</tr>";
}
?>

</table>
</body>
</html>
