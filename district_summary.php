<?php
include("includes/connect.php");
include("includes/functions.php");
if (!isset($_COOKIE['year']))
    header("location:first.php?do=enter_year");
$year = $_COOKIE['year'];
//$sturec = mysql_query("SELECT * FROM `main` where `year`=$year AND `school`='$school'  order by sn");
//$nor = mysql_num_rows($sturec);
$subjects = mysql_query("SELECT * from subject where `year`=$year");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>DEO Makawanput, Result Processing System</title>
    <style type="text/css">
        <!--
        * {
            margin: 0px;
            font-size: 10pt;
        }

        h1 {
            font-size: 16pt;
        }

        h2 {
            font-size: 12pt;
        }

        h3 {
            font-size: 16pt;
        }

        .aa th, .aa td {
            border: #AAAAAA thin solid;
            border-collapse: collapse;
        }

        .aa table, .aa td, .aa th, .aa tr {
            border: #AAAAAA thin solid;
            border-collapse: collapse;
        }

        .mydiv {
            width: 11in;
            height: 8in;
            page-break-before: always;
        }

        -->
    </style>
</head>

<body>
<div class="mydiv">

    <h3>Municipal Summary</h3>
    <br/>
    <a href="first.php?do=doitdo" style="text-decoration:none;color:black;">
        <table border="1" cellspacing="0" cellpadding="5" class="myclass">
            <caption>Overall Summary</caption>
            <tr>
                <th rowspan="2">Total</th>
                <th colspan="2">Passed</th>
                <th colspan="2">Failed</th>
                <th colspan="2">A+</th>
                <th colspan="2">A</th>
                <th colspan="2">B+</th>
                <th colspan="2">B</th>
                <th colspan="2">C+</th>
                <th colspan="2">C</th>
                <th colspan="2">D+</th>
                <th colspan="2">D</th>
                <th colspan="2">E</th>
            </tr>
            <tr>
                <th>No.</th>
                <th>%</th>
                <th>No.</th>
                <th>%</th>
                <th>No.</th>
                <th>%</th>
                <th>No.</th>
                <th>%</th>
                <th>No.</th>
                <th>%</th>
                <th>No.</th>
                <th>%</th>
                <th>No.</th>
                <th>%</th>
                <th>No.</th>
                <th>%</th>
                <th>No.</th>
                <th>%</th>
                <th>No.</th>
                <th>%</th>
                <th>No.</th>
                <th>%</th>
            </tr>
            <?php $return = result_overalld($year); ?>
            <tr>
                <?php
                $total = $return['total'];
                $fail = $return['fail'];
                $pass = $total - $fail;
                ?>
                <th><?php echo $total; ?></TH>
                <th><?php echo $pass; ?></th>
                <th><?php echo round(($pass / $total) * 100, 2); ?> %</th>
                <th><?php echo $fail; ?></th>
                <th><?php echo round(($fail / $total) * 100, 2); ?> %</th>
                <th><?php echo $return['ap']; ?></th>
                <th><?php echo round(($return['ap'] / $total) * 100, 2); ?> %</th>
                <th><?php echo $return['a']; ?></th>
                <th><?php echo round(($return['a'] / $total) * 100, 2); ?> %</th>
                <th><?php echo $return['bp']; ?></th>
                <th><?php echo round(($return['bp'] / $total) * 100, 2); ?> %</th>
                <th><?php echo $return['b']; ?></th>
                <th><?php echo round(($return['b'] / $total) * 100, 2); ?> %</th>
                <th><?php echo $return['cp']; ?></th>
                <th><?php echo round(($return['cp'] / $total) * 100, 2); ?> %</th>
                <th><?php echo $return['c']; ?></th>
                <th><?php echo round(($return['c'] / $total) * 100, 2); ?> %</th>
                <th><?php echo $return['dp']; ?></th>
                <th><?php echo round(($return['dp'] / $total) * 100, 2); ?> %</th>
                <th><?php echo $return['d']; ?></th>
                <th><?php echo round(($return['d'] / $total) * 100, 2); ?> %</th>
                <th><?php echo $return['e']; ?></th>
                <th><?php echo round(($return['e'] / $total) * 100, 2); ?> %</th>





            </tr>
        </table>
    </a>


</div>

</body>
</html>
