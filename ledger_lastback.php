<?php
include("includes/connect.php");
include("includes/functions.php");
if(!isset($_COOKIE['school']) && !isset($_COOKIE['year']))
header("location:index.php?do=enter_year");
$school=$_COOKIE['school'];
$year=$_COOKIE['year'];
$sturec=mysql_query("SELECT * FROM `main` where `year`=$year AND `school`='$school' order by sn");
$nor=mysql_num_rows($sturec);
$subjects=mysql_query("SELECT * from subject where `year`=$year");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DEO Makawanput, Result Processing System</title>
<style type="text/css">
<!--
*
{
margin:0px;
font-size:10pt;
}
h1
{
font-size:16pt;
}
h2
{
font-size:12pt;
}
h3
{
font-size:16pt;
}
.aa th, .aa td
{
border:#AAAAAA thin solid;
border-collapse:collapse;
}
.aa table, .aa td, .aa th, .aa tr
{
border:#AAAAAA thin solid;
border-collapse:collapse;
}
.mydiv
{
width:11in;
height:8in;
page-break-before:always;
}
.myclass
{
margin:auto;
}
-->
</style>
</head>

<body>
<div class="mydiv">

<H3 align="center">District Level Examination Board<br>Basic Level Final Examination - <?php echo $year; ?><br>
<?php echo schoolfromid($school);?><br />
School code: <?php echo $school;?></H3><br />
<br />
<a href="first.php?do=doitdo" style="text-decoration:none;color:black">

<table border="1" cellspacing="0" cellpadding="5" class="myclass" align="center">
<caption>Overall Summary</caption>
<tr><th rowspan="2">Total</th><th colspan="2">Passed</th><th colspan="2">Failed</th><th colspan="2">Distinction</th><th colspan="2">1<sup>st</sup></th><th colspan="2">2<sup>nd</sup></th><th colspan="2">3<sup>rd</sup></th></tr>
<tr><th>No.</th><th>%</th><th>No.</th><th>%</th><th>No.</th><th>%</th><th>No.</th><th>%</th><th>No.</th><th>%</th><th>No.</th><th>%</th></tr>
<?php $rr=result_overall($school,$year);?>
<tr>
<?php
$total=$rr[5];
$fail=$rr[4];
$pass=$total-$fail;
$dist=$rr[0];
$first=$rr[1];
$sec=$rr[2];
$thi=$rr[3];
$p=$rr[5]-$rr[4];
$calc=$p/$rr[5]*100;
?>
<th><?php echo $total;?></TH>
<th><?php echo $pass;?></th>
<th><?php echo round(($pass/$total)*100,2);?> %</th>
<th><?php echo $fail;?></th>
<th><?php echo round(($fail/$total)*100,2);?> %</th>
<th><?php echo $dist;?></th>
<th><?php echo round(($dist/$total)*100,2);?> %</th>
<th><?php echo $first;?></th>
<th><?php echo round(($first/$total)*100,2);?> %</th>
<th><?php echo $sec;?></th>
<th><?php echo round(($sec/$total)*100,2);?> %</th>
<th><?php echo $thi;?></th>
<th><?php echo round(($thi/$total)*100,2);?> %</th>
</tr></table>
</a>
<table border="1" style="margin:auto;" cellspacing="0" cellpadding="5">
<caption>Subject Summary</caption>
<?php
$data=mysql_fetch_row($subjects);
$no=$data[2];
echo "<tr><th>Subject</th>";
for($i=3;$i<=$no+2;$i++)
{
echo "<th colspan=3>".$data[$i]."</TH>";
}
echo "</tr>";
$xxxx=1;
echo "<th>Highest</th>";
for($i=3;$i<=$no+2;$i++)
{
echo "<th colspan=3>".high($xxxx++,$year,$school)."</TH>";
}
echo "</tr>";
echo "<th>Lowest</th>";
$xxxx=1;
for($i=3;$i<=$no+2;$i++)
{
echo "<th colspan=3>".low($xxxx++,$year,$school)."</TH>";
}
echo "</tr>";
echo "<th>Average</th>";
$xxxx=1;
for($i=3;$i<=$no+2;$i++)
{
echo "<th colspan=3>".avrg($xxxx++,$year,$school)."</TH>";
}
echo "</tr>";
echo "<th>Subject Total</th>";
$xxxx=1;
for($i=3;$i<=$no+2;$i++)
{
echo "<th colspan=3>".jod($xxxx++,$year,$school)."</TH>";
}
echo "</tr>";
echo "<th>District Highest</th>";
$xxxx=1;
for($i=3;$i<=$no+2;$i++)
{
echo "<th colspan=3>".district($xxxx++,$year)."</TH>";
}
echo "</tr>";
?>
</table>
<table border="1" cellspacing="0" cellpadding="5" style="margin:auto;">
<caption>Topper Info</caption>
<tr>
<th>Category</th><th>Symbol No.</th><th>Name</th><th>Percentage</th>
</tr>
<tr>
<th>School Topper </th><th><?php $x=topper($year,$school); echo $x[0];?></th>
<th><?php echo $x[1];?></th>
<th><?php echo $x[2];?></th>
</tr>
<tr>
<th>Topper(Boy) </th><th><?php $x=btopper($year,$school); echo $x[0];?></th>
<th><?php echo $x[1];?></th>
<th><?php echo $x[2];?></th>
</tr>
<tr>
<th>Topper(Girl) </th><th><?php $x=gtopper($year,$school); echo $x[0];?></th>
<th><?php echo $x[1];?></th>
<th><?php echo $x[2];?></th>
</tr>
</table>
<br>
<table border=1 cellpadding=5 cellspacing=0 style="margin:auto;">
<tr>
<th>District Topper:</th><th><?php $x=dtopper($year); echo $x[0][0];?></th>
<th><?php echo $x[0][1];?></th>
<th><?php echo $x[0][2];?></th>
<th><?php echo schoolfromid($x[0][3]);?></th>
</tr>
<tr>
<th>District Topper:</th><th><?php $x=dtopper($year); echo $x[1][0];?></th>
<th><?php echo $x[1][1];?></th>
<th><?php echo $x[1][2];?></th>
<th><?php echo schoolfromid($x[1][3]);?></th>
</tr>
<tr>
<th>District Topper:</th><th><?php $x=dtopper($year); echo $x[2][0];?></th>
<th><?php echo $x[2][1];?></th>
<th><?php echo $x[2][2];?></th>
<th><?php echo schoolfromid($x[2][3]);?></th>
</tr>

</table>
</div>
<?php
$counter=1;
while($ddata=mysql_fetch_row($sturec))
{
			if($counter++ % 30 ==1)
			{
			?>
			<div class="mydiv">
			<table cellspacing="0" cellpadding="2" width="100%" class="aa">
			<tr>
			<th rowspan="2">SN</th>
			<th rowspan="2">Name</th>
			<th rowspan="2">DOB</th>
			<?php
			for($i=3;$i<=$no+2;$i++)
			echo "<th colspan=3>".substr($data[$i],0,8)."</TH>";
			?>
			<th rowspan="2">Tot</th>
			<th rowspan="2">%</th>
			<th rowspan="2">Result</th>
			<th rowspan="2">Rem</th>
			</tr>
			<tr>
			<?php
			for($i=1;$i<=$no;$i++)
			echo "<td><font size=2>TH</font></td><td><font size=2>PR</font></td><td><font size=2>To</font></td>";
			?>
			</tr>
			<?php
			}
			echo "<tr>";
			echo "<td>".$ddata[1]."</td>";
			echo "<th align=left>".$ddata[2]."</th>";
			echo "<th align=left>".$ddata[4]."</th>";
			$rowcount=6;
			$tot=0;
			$res="PASS";
			for($i=1;$i<=$no;$i++)
			{
			$first=$ddata[$rowcount++];
			$tot+=$first;
			echo "<td>".$first."</td><td>";
			$second=$ddata[$rowcount++];
			$tot+=$second;
			if($second==0)
			echo "&nbsp;";
			else
			echo $second;
			$tt=$first+$second;
			echo "</td><td>".$tt."</td>";
			if($tt<=32)
			$res="FAIL";
			}
			echo "<td>".$tot."</td>";
			echo "<td>".round((($tot/givetotal($year))*100),2)."</td>";
			echo "<td>".$ddata[28]."</td>";
			echo "<td>";
			echo $ddata[29];
			echo "</td>";
			echo "</tr>";
			if($counter % 30 ==1)
			{
			echo "</table></div>";
			}
}
?>
<?php
if($counter % 30 >= 1)
echo "</table></div>";
?>
</body>
</html>
