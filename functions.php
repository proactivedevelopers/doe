<?php
include_once("connect.php");
function getmarks($year, $school, $sn)
{
    $squel = mysql_query("Select * from main where school='$school' and year='$year' and sn='$sn'");
    $sqdata = mysql_fetch_assoc($squel);
    $no = getsub($year);
    for ($i = 1; $i <= $no; $i++) {
        $mark = "sub" . $i;
        $mark1 = "sub" . $i . "1";
        $dd = $sqdata[$mark] + $sqdata[$mark1];
        $ret[$i] = $dd;
    }
    return $ret;
}


function getGPA($n)
{
    if ($n >= 0 && $n < 20) {
        $return[0] = 0;
        $return[1] = 'E';
    } elseif ($n >= 20 && $n < 30) {
        $return[0] = 0.8;
        $return[1] = 'D';
    } elseif ($n >= 30 & $n < 40) {
        $return[0] = 1.6;
        $return[1] = 'D+';
    } elseif ($n >= 40 & $n < 50) {
        $return[0] = 2.0;
        $return[1] = 'C';
    } elseif ($n >= 50 & $n < 60) {
        $return[0] = 2.4;
        $return[1] = 'C+';
    } elseif ($n >= 60 & $n < 70) {
        $return[0] = 2.8;
        $return[1] = 'B';
    } elseif ($n >= 70 & $n < 80) {
        $return[0] = 3.2;
        $return[1] = 'B+';
    } elseif ($n >= 80 & $n < 90) {
        $return[0] = 3.6;
        $return[1] = 'A';
    } else {
        $return[0] = 4.0;
        $return[1] = 'A+';
    }
    return $return;
}

function tellresult($the, $prac, $no, $year, $type)
{
    $gr = isgrace($the, $prac, $no, $year, $type);
    if ($gr == 'p') {
        $res[0] = "Passed";
        $res[1] = 0;
    } elseif ($gr == 'f') {
        $res[0] = "Failed";
        $res[1] = 0;
    } else {
        $res[0] = "Passed";
        $res[1] = $gr;
    }

    return $res;
}

function isgrace($b, $pb, $no, $year, $type)
{

    $passpercentage = mysql_result(mysql_query("select * from curriculum"), 0, 1);
    $need = 0;
    $fqu = "Select sub1,sub11";
    for ($i = 1; $i <= $no - 1; $i++) {
        $aaa = $i + 1;
        $fqu .= ",sub" . $aaa . ",sub" . $aaa . "1";
    }
    $fqu .= " from fmpm where year=$year;";
    $query = mysql_query($fqu) or die(mysql_error());
    $fqu = mysql_fetch_row($query) or die(mysql_error());
    $grac = mysql_query("select marks from grace where year=$year and type='$type'");
    $gracc = mysql_fetch_row($grac);
    $r = "P";
    for ($i = 0, $x = 0; $i <= ($no - 1); $i++) {
        $pass = ceil(($fqu[$x++] * $passpercentage) / 100);
        $tpass = floor(($fqu[$x++] * $passpercentage) / 100);
        if ($b[$i] < $pass) {
            if ($b[$i] == 0)
                $need = 100;
            $need = $need + $pass - $b[$i];
            $r = "F";
        }
        if ($pb[$i] < $tpass) {
            $need = $need + $tpass - $pb[$i];
            $r = "F";
        }
    }
    if ($r == "P")
        return "p";
    elseif ($r == "F") {
        if ($need <= $gracc[0])
            return $need;
        else
            return "f";
    }
}

function getsub($year)
{
    $sque = mysql_query("Select no From subject where year=$year");
    $abc = mysql_fetch_row($sque);
    return $abc[0];
}

function isprac($sub, $year)
{
    $s = "sub" . $sub . "1";
    $query = mysql_query("select $s from fmpm where year=$year") or die(mysql_error());
    return mysql_result($query, 0, 0);
}

function subject($a, $year)
{
    $fi = "sub" . $a;
    $give = mysql_query("select $fi from subject where year=$year");
    return mysql_result($give, 0, 0);
}

function putmarks($a, $result, $marks)
{
    $passpercentage = mysql_result(mysql_query("select * from curriculum"), 0, 1);
    if ($result == "Failed")
        if (isgrace($marks))
            return $passpercentage;
        else
            return $a;
    else
        return $a;
}

function writespace($a)
{
    for ($i = 1; $i <= $a; $i++)
        echo "&nbsp;";
    return;
}

function chkpass($pas, $user, $table)
{
    $pass = sha1($pas);
    $sqll = mysql_query("Select auth_id from `$table` where password='$pass' and username='$user'");
    if (mysql_num_rows($sqll) >= 1)
        return true;
    else
        return false;
}

function changpass($old, $new)
{
    $old = sha1($old);
    $new = sha1($new);
    if (mysql_query("Update auth set password='$new' where password='$old'"))
        return true;
    else
        return false;
}

function result_overallback($school, $year)
{
    $a = mysql_query("Select count(sn) from main where school='$school' and year=$year");
    $b = mysql_query("Select count(sn) from main where school='$school' and per>=80 and result='Passed'  and year=$year");
    $c = mysql_query("Select count(sn) from main where school='$school' and per>=60 and per<80 and result='Passed' and year=$year");
    $d = mysql_query("Select count(sn) from main where school='$school' and per>=45 and per<60 and result='Passed'  and year=$year");
    $e = mysql_query("Select count(sn) from main where school='$school' and per>=32 and per<45 and result='Passed'  and year=$year");
    $f = mysql_query("Select count(sn) from main where school='$school' and result='Failed'  and year=$year");
    $rv[5] = mysql_result($a, 0, 0);
    $rv[0] = mysql_result($b, 0, 0);
    $rv[1] = mysql_result($c, 0, 0);
    $rv[2] = mysql_result($d, 0, 0);
    $rv[3] = mysql_result($e, 0, 0);
    $rv[4] = mysql_result($f, 0, 0);
    return $rv;
}

function result_overalld($year)
{
    $total = mysql_query("Select count(sn) from maingrade where year=$year");
    $fail = mysql_query("Select count(sn) from maingrade where weight<40 and year=$year");
    $ap = mysql_query("Select count(sn) from maingrade where gpa = 'A+' and year=$year");
    $a = mysql_query("Select count(sn) from maingrade where gpa = 'A' and year=$year");
    $bp = mysql_query("Select count(sn) from maingrade where gpa = 'B+' and year=$year");
    $b = mysql_query("Select count(sn) from maingrade where gpa = 'B' and year=$year");
    $cp = mysql_query("Select count(sn) from maingrade where gpa = 'C+' and year=$year");
    $c = mysql_query("Select count(sn) from maingrade where gpa = 'C' and year=$year");
    $dp = mysql_query("Select count(sn) from maingrade where gpa = 'D+' and year=$year");
    $d = mysql_query("Select count(sn) from maingrade where gpa = 'D' and year=$year");
    $e = mysql_query("Select count(sn) from maingrade where gpa = 'E' and year=$year");
    $return['ap'] = mysql_result($ap, 0, 0);
    $return['a'] = mysql_result($a, 0, 0);
    $return['bp'] = mysql_result($bp, 0, 0);
    $return['b'] = mysql_result($b, 0, 0);
    $return['cp'] = mysql_result($cp, 0, 0);
    $return['c'] = mysql_result($c, 0, 0);
    $return['dp'] = mysql_result($dp, 0, 0);
    $return['d'] = mysql_result($d, 0, 0);
    $return['e'] = mysql_result($e, 0, 0);
    $return['fail'] = mysql_result($fail, 0, 0);
    $return['total'] = mysql_result($total, 0, 0);

    return $return;
}

function result_overall($school,$year)
{
    $total = mysql_query("Select count(sn) from maingrade where year=$year and school='".$school."'");
    $fail = mysql_query("Select count(sn) from maingrade where weight<40 and year=$year and school='".$school."'");
    $ap = mysql_query("Select count(sn) from maingrade where gpa = 'A+' and year=$year and school='".$school."'");
    $a = mysql_query("Select count(sn) from maingrade where gpa = 'A' and year=$year and school='".$school."'");
    $bp = mysql_query("Select count(sn) from maingrade where gpa = 'B+' and year=$year and school='".$school."'");
    $b = mysql_query("Select count(sn) from maingrade where gpa = 'B' and year=$year and school='".$school."'");
    $cp = mysql_query("Select count(sn) from maingrade where gpa = 'C+' and year=$year and school='".$school."'");
    $c = mysql_query("Select count(sn) from maingrade where gpa = 'C' and year=$year and school='".$school."'");
    $dp = mysql_query("Select count(sn) from maingrade where gpa = 'D+' and year=$year and school='".$school."'");
    $d = mysql_query("Select count(sn) from maingrade where gpa = 'D' and year=$year and school='".$school."'");
    $e = mysql_query("Select count(sn) from maingrade where gpa = 'E' and year=$year and school='".$school."'");
    $return['ap'] = mysql_result($ap, 0, 0);
    $return['a'] = mysql_result($a, 0, 0);
    $return['bp'] = mysql_result($bp, 0, 0);
    $return['b'] = mysql_result($b, 0, 0);
    $return['cp'] = mysql_result($cp, 0, 0);
    $return['c'] = mysql_result($c, 0, 0);
    $return['dp'] = mysql_result($dp, 0, 0);
    $return['d'] = mysql_result($d, 0, 0);
    $return['e'] = mysql_result($e, 0, 0);
    $return['fail'] = mysql_result($fail, 0, 0);
    $return['total'] = mysql_result($total, 0, 0);

    return $return;
}

function high($subject, $year, $school)
{
    $ssu = "sub" . $subject;
    $ssu1 = "sub" . $subject . "1";
    $ss = mysql_query("select max($ssu+$ssu1) from main where year=$year and school='$school'");
    $x = mysql_result($ss, 0, 0);
    return $x;
}

function highd($subject, $year)
{
    $ssu = "sub" . $subject;
    $ssu1 = "sub" . $subject . "1";
    $ss = mysql_query("select max($ssu+$ssu1) from main where year=$year");
    $x = mysql_result($ss, 0, 0);
    return $x;
}

function district($subject, $year)
{
    $ssu = "sub" . $subject;
    $ssu1 = "sub" . $subject . "1";
    $ss = mysql_query("select max($ssu+$ssu1) from main where year=$year");
    $x = mysql_result($ss, 0, 0);
    return $x;
}

function low($subject, $year, $school)
{
    $ssu = "sub" . $subject;
    $ssu1 = "sub" . $subject . "1";
    $ss = mysql_query("select min($ssu+$ssu1) from main where year=$year and school='$school' and $ssu>0");
    $x = mysql_result($ss, 0, 0);
    return $x;
}

function lowd($subject, $year)
{
    $ssu = "sub" . $subject;
    $ssu1 = "sub" . $subject . "1";
    $ss = mysql_query("select min($ssu+$ssu1) from main where year=$year and $ssu>0");
    $x = mysql_result($ss, 0, 0);
    return $x;
}

function avrg($subject, $year, $school)
{
    $ssu = "sub" . $subject;
    $ssu1 = "sub" . $subject . "1";
    $ss = mysql_query("select round(avg($ssu+$ssu1),2) from main where year=$year and school='$school'");
    $x = mysql_result($ss, 0, 0);
    return $x;
}

function avrgd($subject, $year)
{
    $ssu = "sub" . $subject;
    $ssu1 = "sub" . $subject . "1";
    $ss = mysql_query("select round(avg($ssu+$ssu1),2) from main where year=$year");
    $x = mysql_result($ss, 0, 0);
    return $x;
}

function jod($subject, $year, $school)
{
    $ssu = "sub" . $subject;
    $ssu1 = "sub" . $subject . "1";
    $ss = mysql_query("select sum($ssu+$ssu1) from main where year=$year and school='$school'");
    $x = mysql_result($ss, 0, 0);
    return $x;
}

function jodd($subject, $year)
{
    $ssu = "sub" . $subject;
    $ssu1 = "sub" . $subject . "1";
    $ss = mysql_query("select sum($ssu+$ssu1) from main where year=$year");
    $x = mysql_result($ss, 0, 0);
    return $x;
}

function topper($year, $school)
{
    $ss = mysql_query("select sn,name,per from main where year=$year and school='$school' group by per DESC");
    $x[0] = mysql_result($ss, 0, 0);
    $x[1] = mysql_result($ss, 0, 1);
    $x[2] = mysql_result($ss, 0, 2);
    return $x;
}

function dtopper($year)
{
    $ss = mysql_query("select sn,name,per,school from main where year=$year order by per DESC");
    $x[0][0] = mysql_result($ss, 0, 0);
    $x[0][1] = mysql_result($ss, 0, 1);
    $x[0][2] = mysql_result($ss, 0, 2);
    $x[0][3] = mysql_result($ss, 0, 3);
    $x[1][0] = mysql_result($ss, 1, 0);
    $x[1][1] = mysql_result($ss, 1, 1);
    $x[1][2] = mysql_result($ss, 1, 2);
    $x[1][3] = mysql_result($ss, 1, 3);
    $x[2][0] = mysql_result($ss, 2, 0);
    $x[2][1] = mysql_result($ss, 2, 1);
    $x[2][2] = mysql_result($ss, 2, 2);
    $x[2][3] = mysql_result($ss, 2, 3);
    $x[3][0] = mysql_result($ss, 3, 0);
    $x[3][1] = mysql_result($ss, 3, 1);
    $x[3][2] = mysql_result($ss, 3, 2);
    $x[3][3] = mysql_result($ss, 3, 3);
    $x[4][0] = mysql_result($ss, 4, 0);
    $x[4][1] = mysql_result($ss, 4, 1);
    $x[4][2] = mysql_result($ss, 4, 2);
    $x[4][3] = mysql_result($ss, 4, 3);
    return $x;
}

function dtopperi($year)
{
    $ss = mysql_query("select main.sn,main.name,main.per,main.school from main inner join schools on main.school=schools.schoolcode and schools.type=0 and main.year=$year order by per DESC");
    $x[0][0] = mysql_result($ss, 0, 0);
    $x[0][1] = mysql_result($ss, 0, 1);
    $x[0][2] = mysql_result($ss, 0, 2);
    $x[0][3] = mysql_result($ss, 0, 3);
    $x[1][0] = mysql_result($ss, 1, 0);
    $x[1][1] = mysql_result($ss, 1, 1);
    $x[1][2] = mysql_result($ss, 1, 2);
    $x[1][3] = mysql_result($ss, 1, 3);
    $x[2][0] = mysql_result($ss, 2, 0);
    $x[2][1] = mysql_result($ss, 2, 1);
    $x[2][2] = mysql_result($ss, 2, 2);
    $x[2][3] = mysql_result($ss, 2, 3);
    $x[3][0] = mysql_result($ss, 3, 0);
    $x[3][1] = mysql_result($ss, 3, 1);
    $x[3][2] = mysql_result($ss, 3, 2);
    $x[3][3] = mysql_result($ss, 3, 3);
    $x[4][0] = mysql_result($ss, 4, 0);
    $x[4][1] = mysql_result($ss, 4, 1);
    $x[4][2] = mysql_result($ss, 4, 2);
    $x[4][3] = mysql_result($ss, 4, 3);
    return $x;
}

function dtopperiboy($year)
{
    $ss = mysql_query("select main.sn,main.name,main.per,main.school from main inner join schools on main.school=schools.schoolcode and schools.type=0 and main.year=$year and main.gender like 'm%' order by per DESC LIMIT 0,1");
    $x[0][0] = mysql_result($ss, 0, 0);
    $x[0][1] = mysql_result($ss, 0, 1);
    $x[0][2] = mysql_result($ss, 0, 2);
    $x[0][3] = mysql_result($ss, 0, 3);
    return $x;
}

function dtopperigirl($year)
{
    $ss = mysql_query("select main.sn,main.name,main.per,main.school from main inner join schools on main.school=schools.schoolcode and schools.type=0 and main.year=$year and main.gender like 'f%' order by per DESC LIMIT 0,1");
    $x[0][0] = mysql_result($ss, 0, 0);
    $x[0][1] = mysql_result($ss, 0, 1);
    $x[0][2] = mysql_result($ss, 0, 2);
    $x[0][3] = mysql_result($ss, 0, 3);
    return $x;
}

function grades(){
    $return[] = "A+";
    $return[] = "A";
    $return[] = "B+";
    $return[] = "B";
    $return[] = "C+";
    $return[] = "C";
    $return[] = "D+";
    $return[] = "D";
    $return[] = "E+";
    $return[] = "E";
    return $return;
}

function dtopperjboy($year)
{
    $ss = mysql_query("select main.sn,main.name,main.per,main.school from main inner join schools on main.school=schools.schoolcode and schools.type=1 and main.year=$year and gender like 'm%' order by per DESC LIMIT 0,1");
    $x[0][0] = mysql_result($ss, 0, 0);
    $x[0][1] = mysql_result($ss, 0, 1);
    $x[0][2] = mysql_result($ss, 0, 2);
    $x[0][3] = mysql_result($ss, 0, 3);
    return $x;
}

function dtopperjgirl($year)
{
    $ss = mysql_query("select main.sn,main.name,main.per,main.school from main inner join schools on main.school=schools.schoolcode and schools.type=1 and main.year=$year and gender like 'f%' order by per DESC LIMIT 0,2");
    $x[0][0] = mysql_result($ss, 0, 0);
    $x[0][1] = mysql_result($ss, 0, 1);
    $x[0][2] = mysql_result($ss, 0, 2);
    $x[0][3] = mysql_result($ss, 0, 3);
    $x[1][0] = mysql_result($ss, 1, 0);
    $x[1][1] = mysql_result($ss, 1, 1);
    $x[1][2] = mysql_result($ss, 1, 2);
    $x[1][3] = mysql_result($ss, 1, 3);
    return $x;
}

function dtopperj($year)
{
    $ss = mysql_query("select main.sn,main.name,main.per,main.school from main inner join schools on main.school=schools.schoolcode and schools.type=1 and main.year=$year order by per DESC");
    $x[0][0] = mysql_result($ss, 0, 0);
    $x[0][1] = mysql_result($ss, 0, 1);
    $x[0][2] = mysql_result($ss, 0, 2);
    $x[0][3] = mysql_result($ss, 0, 3);
    $x[1][0] = mysql_result($ss, 1, 0);
    $x[1][1] = mysql_result($ss, 1, 1);
    $x[1][2] = mysql_result($ss, 1, 2);
    $x[1][3] = mysql_result($ss, 1, 3);
    $x[2][0] = mysql_result($ss, 2, 0);
    $x[2][1] = mysql_result($ss, 2, 1);
    $x[2][2] = mysql_result($ss, 2, 2);
    $x[2][3] = mysql_result($ss, 2, 3);
    $x[3][0] = mysql_result($ss, 3, 0);
    $x[3][1] = mysql_result($ss, 3, 1);
    $x[3][2] = mysql_result($ss, 3, 2);
    $x[3][3] = mysql_result($ss, 3, 3);
    $x[4][0] = mysql_result($ss, 4, 0);
    $x[4][1] = mysql_result($ss, 4, 1);
    $x[4][2] = mysql_result($ss, 4, 2);
    $x[4][3] = mysql_result($ss, 4, 3);
    return $x;
}

function btopper($year, $school)
{
    $ss = mysql_query("select sn,name,gradepoint from maingrade where year=$year and school='$school' and left(gender,1)='M' order by weight DESC");
    if (mysql_num_rows($ss) > 0) {
        $x[0] = mysql_result($ss, 0, 0);
        $x[1] = mysql_result($ss, 0, 1);
        $x[2] = mysql_result($ss, 0, 2);
    } else {
        $x[0] = "N/A";
        $x[1] = "N/A";
        $x[2] = "N/A";
    }
    return $x;
}

function gtopper($year, $school)
{
    $ss = mysql_query("select sn,name,gradepoint from maingrade where year=$year and school='$school' and left(gender,1)='F' order by weight DESC");
    if (mysql_num_rows($ss) > 0) {
        $x[0] = mysql_result($ss, 0, 0);
        $x[1] = mysql_result($ss, 0, 1);
        $x[2] = mysql_result($ss, 0, 2);
    } else {
        $x[0] = "N/A";
        $x[1] = "N/A";
        $x[2] = "N/A";
    }
    return $x;
}


function getfmth($year)
{
    $tq = mysql_query("select * from fmpm where year=$year");
    $tt = mysql_fetch_row($tq);
    $no = getsub($year);
    $a = 0;
    $th = 1;
    for ($i = 1; $i <= $no; $i++) {
        $x[$a] = $tt[$th];
        $th = $th + 2;
        $a++;
    }
    return $x;
}

function getfmpr($year)
{
    $tq = mysql_query("select * from fmpm where year=$year");
    $tt = mysql_fetch_row($tq);
    $no = getsub($year);
    $a = 0;
    $th = 2;
    for ($i = 1; $i <= $no; $i++) {
        $x[$a] = $tt[$th];
        $th = $th + 2;
        $a++;
    }
    return $x;
}

function givsub($year)
{
    $no = getsub($year);
    $sq = mysql_query("select * from subject where year=$year");
    $ssq = mysql_fetch_row($sq);
    for ($i = 1; $i <= $no; $i++) {
        $x[$i - 1] = $ssq[$i + 2];
    }
    return $x;
}

function getmarkth($year, $school, $sn)
{
    $no = getsub($year);
    $ss = mysql_query("select * from main where year=$year and school='$school' and sn='$sn'");
    $sss = mysql_fetch_row($ss);
    $a = 6;
    for ($i = 1; $i <= $no; $i++) {
        $x[$i - 1] = $sss[$a];
        $a = $a + 2;
    }
    return $x;
}

function getmarkpr($year, $school, $sn)
{
    $no = getsub($year);
    $ss = mysql_query("select * from main where year=$year and school='$school' and sn='$sn'");
    $sss = mysql_fetch_row($ss);
    $a = 7;
    for ($i = 1; $i <= $no; $i++) {
        $x[$i - 1] = $sss[$a];
        $a = $a + 2;
    }
    return $x;
}

function checkdup($year, $sn)
{
    $q = mysql_query("select * from main where year=$year and sn='$sn'");
    if (mysql_num_rows($q) > 0)
        return 1;
    else
        return 0;
}

/*
$aa=getmarks(2059,"ABC School",4534);
print_r($aa);
echo "<br>".isgrace($aa);
echo "<br>".putmarks(26,"Failed",$aa);
*/

function idfromschool($school)
{
    $data = mysql_query("select schoolcode from schools where name='$school'");
    $d = mysql_fetch_row($data);
    return $d[0];
}

function schoolfromid($id)
{
    $data = mysql_query("select name from schools where schoolcode='$id'");
    $d = mysql_fetch_row($data);
    return $d[0];
}


function givetotal($year)
{
    $que = mysql_query("Select * from fmpm where year=$year");
    $dat = mysql_fetch_row($que);
    return $dat[1] + $dat[2] + $dat[3] + $dat[4] + $dat[5] + $dat[6] + $dat[7] + $dat[8] + $dat[9] + $dat[10] + $dat[11] + $dat[12] + $dat[13] + $dat[14] + $dat[15] + $dat[16] + $dat[17] + $dat[18] + $dat[19] + $dat[20];
}

?>