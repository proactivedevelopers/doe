<?php
include("includes/connect.php");
include("includes/functions.php");
if (!isset($_COOKIE['school']) && !isset($_COOKIE['year']))
    header("location:index.php?do=enter_year");
$school = $_COOKIE['school'];
$year = $_COOKIE['year'];
$sturec = mysql_query("SELECT * FROM `main` where `year`=$year AND `school`='$school' order by sn ASC");
$nor = mysql_num_rows($sturec);
$subjects = mysql_query("SELECT * from subject where `year`=$year");
$passpercentage = mysql_result(mysql_query("select * from curriculum"), 0, 1);
$data = mysql_fetch_row($subjects);
$no = $data[2];
$number_of_records = 20;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>DEO Makawanpur, Result Processing System</title>
    <style type="text/css">
        <!--
        * {
            margin: 0px;
            font-size: 10pt;
        }

        h1 {
            font-size: 16pt;
        }

        h2 {
            font-size: 12pt;
        }

        h3 {
            font-size: 16pt;
        }

        .aa th, .aa td {
            border: #AAAAAA thin solid;
            border-collapse: collapse;
        }

        .aa table, .aa td, .aa th, .aa tr {
            border: #AAAAAA thin solid;
            border-collapse: collapse;
        }

        .mydiv {
            width: 22in;
            height: 8in;
            page-break-before: always;
        }

        .myclass {
            margin: auto;
        }

        -->
    </style>
</head>

<body>
<div class="mydiv">
    <h3 align="center">Hetauda Sub-Metropolitan City</h3>
    <h3 align="center">Office of The Municipal Executive</h3>
    <H3 align="center">Basic Level Education Completion Examination - <?php echo $year; ?><br>
        <?php echo schoolfromid($school); ?><br/>
        School code: <?php echo $school; ?></H3>
    <a href="first.php?do=doitdo" style="text-decoration:none;color:black">
        <table border="1" cellspacing="0" cellpadding="5" class="myclass" align="center">
            <caption>Overall Summary</caption>
            <tr>
                <th rowspan="2">Total</th>
                <th colspan="2">A+</th>
                <th colspan="2">A</th>
                <th colspan="2">B+</th>
                <th colspan="2">B</th>
                <th colspan="2">C+</th>
                <th colspan="2">C</th>
                <th colspan="2">D+</th>
                <th colspan="2">D</th>
                <th colspan="2">E</th>
            </tr>
            <tr>
                <th>No.</th>
                <th>%</th>
                <th>No.</th>
                <th>%</th>
                <th>No.</th>
                <th>%</th>
                <th>No.</th>
                <th>%</th>
                <th>No.</th>
                <th>%</th>
                <th>No.</th>
                <th>%</th>
                <th>No.</th>
                <th>%</th>
                <th>No.</th>
                <th>%</th>
                <th>No.</th>
                <th>%</th>
            </tr>
            <?php $return = result_overall($school,$year); ?>
            <tr>
                <?php
                $total = $return['total'];
                $fail = $return['fail'];
                $pass = $total - $fail;
                ?>
                <th><?php echo $total; ?></TH>
                <th><?php echo $return['ap']; ?></th>
                <th><?php echo round(($return['ap'] / $total) * 100, 2); ?> %</th>
                <th><?php echo $return['a']; ?></th>
                <th><?php echo round(($return['a'] / $total) * 100, 2); ?> %</th>
                <th><?php echo $return['bp']; ?></th>
                <th><?php echo round(($return['bp'] / $total) * 100, 2); ?> %</th>
                <th><?php echo $return['b']; ?></th>
                <th><?php echo round(($return['b'] / $total) * 100, 2); ?> %</th>
                <th><?php echo $return['cp']; ?></th>
                <th><?php echo round(($return['cp'] / $total) * 100, 2); ?> %</th>
                <th><?php echo $return['c']; ?></th>
                <th><?php echo round(($return['c'] / $total) * 100, 2); ?> %</th>
                <th><?php echo $return['dp']; ?></th>
                <th><?php echo round(($return['dp'] / $total) * 100, 2); ?> %</th>
                <th><?php echo $return['d']; ?></th>
                <th><?php echo round(($return['d'] / $total) * 100, 2); ?> %</th>
                <th><?php echo $return['e']; ?></th>
                <th><?php echo round(($return['e'] / $total) * 100, 2); ?> %</th>
            </tr>

        </table>
    </a>
    <table border="1" cellspacing="0" cellpadding="5" style="margin:auto;">
        <caption>Topper Info</caption>
        <tr>
            <th>Category</th>
            <th>Symbol No.</th>
            <th>Name</th>
            <th>Grade Point</th>
        </tr>
        <tr>
            <th>Topper(Boy)</th>
            <th><?php $x = btopper($year, $school);
                $topQuery = mysql_query("select sn,name from maingrade where year=".$year." and left(gender,1)='M' and school = ".$school." and gradepoint = ".$x[2]);
                while($topInfo = mysql_fetch_assoc($topQuery)){
                    echo $topInfo['sn']."<br>";
                }
            ?>
            </th>
            <th>
                <?php
                $topQuery = mysql_query("select sn,name from maingrade where year=".$year." and left(gender,1)='M' and school = ".$school." and gradepoint = ".$x[2]);
                while($topInfo = mysql_fetch_assoc($topQuery)){
                    echo $topInfo['name']."<br>";
                }
                ?>
            </th>
            <th><?php echo $x[2]; ?></th>
        </tr>
        <tr>
            <th>Topper(Girl)</th>
            <th><?php $x = gtopper($year, $school);
                $topQuery = mysql_query("select sn,name from maingrade where year=".$year." and left(gender,1)='F' and school = ".$school." and gradepoint = ".$x[2]);
                while($topInfo = mysql_fetch_assoc($topQuery)){
                    echo $topInfo['sn']."<br>";
                }
                ?>
                </th>
            <th>
                <?php
                $topQuery = mysql_query("select sn,name from maingrade where year=".$year." and left(gender,1)='F' and school = ".$school." and gradepoint = ".$x[2]);
                while($topInfo = mysql_fetch_assoc($topQuery)){
                    echo $topInfo['name']."<br>";
                }
                ?>
            </th>
            <th><?php echo $x[2]; ?></th>
        </tr>
    </table>

</div>
<?php
$counter = 1;
while ($ddata = mysql_fetch_row($sturec))
{
if ($counter++ % $number_of_records == 1)
{
?>
<div class="mydiv">
    <table cellspacing="0" cellpadding="2" width="100%" class="aa">
        <tr>
            <th rowspan="2">SN</th>
            <th rowspan="2">Name</th>
            <th rowspan="2">DOB</th>
            <th rowspan="2">Father</th>
            <th rowspan="2">Mother</th>
            <?php
            for ($i = 3; $i <= $no + 2; $i++)
                echo "<th colspan=3>" . substr($data[$i], 0, 8) . "</TH>";
            ?>
            <th rowspan="2">Grade</th>
            <th rowspan="2">GPA</th>
            <th rowspan="2">Rem</th>
        </tr>
        <tr>
            <?php
            for ($i = 1; $i <= $no; $i++)
                echo "<td><font size=2>TH</font></td><td><font size=2>PR</font></td><td><font size=2>To</font></td>";
            ?>
        </tr>
        <?php
        }
        $resultG = mysql_fetch_assoc(mysql_query('select * from maingrade where sn = ' . $ddata[1]));

        echo "<tr>";
        echo "<td>" . $ddata[1] . "</td>";
        echo "<th align=left>" . $ddata[2] . "</th>";
        echo "<th align=left>" . $ddata[4] . "</th>";
        echo "<th align=left>" . $ddata[3] . "</th>";
        echo "<th align=left>" . $ddata[31] . "</th>";
        $rowcount = 6;
        $tot = 0;
        $res = "PASS";
        for ($i = 1; $i <= $no; $i++) {
            $first = $ddata[$rowcount++];
            $tot += $first;
            $theoryGPA = getGPA($resultG['sub' . $i]);
            $subjectWeight = $resultG['sub' . $i];
            echo "<td>" . $theoryGPA[1]  . "</td><td>";
            $second = $ddata[$rowcount++];
            $tot += $second;
            if ($second == 0)
                echo "&nbsp;";
            else{
                //echo $second;
                $practical = getGPA($resultG['sub' . $i . '1']);
                echo $practical[1];
                $subjectWeight = ($subjectWeight + $resultG['sub' . $i . '1'])/2;
            }
            $subjectGPA = getGPA($subjectWeight);
            $tt = $first + $second;
            echo "</td><td>".$subjectGPA[1]."</td>";
            if ($tt <= $passpercentage)
                $res = "FAIL";
        }
        echo "<td>" . $resultG['gpa'] . "</td>";
        echo "<td>" . $resultG['gradepoint'] . "</td>";
        echo "<td>";
        echo "</td>";
        echo "</tr>";
        if ($counter % $number_of_records == 1) {
            echo "</table></div>";
        }
        }
        ?>
        <?php
        if ($counter % $number_of_records >= 1)
            echo "</table></div>";
        ?>
</body>
</html>
