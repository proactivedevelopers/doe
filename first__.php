<?php
$do="default";
if(!isset($_COOKIE['password']))
header("location:index.php");
if(isset($_GET['do']))
	$do=$_GET['do'];
else
	$do="default";
include_once("includes/connect.php");
if($do=="enter_rec" || $do=="edit_record" || $do=="select_marksheet" || $do=="view_ledger" || $do=="showind")
	if(!isset($_COOKIE['year']))
	header("location:first.php?do=enter_year");
	elseif(!isset($_COOKIE['school']))
	header("location:first.php?do=enter_sch");
if(isset($_COOKIE['year']))
	$yearr=$_COOKIE['year'];
if(isset($_COOKIE['school']))
	$schooll=$_COOKIE['school'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="style.css" rel="stylesheet" type="text/css" />
<link href="pics/icon.ico" rel="icon" type="image/x-icon" />
<link rel="shortcut icon" href="pics/icon.ico" type="image/x-icon" />
<link rel="stylesheet" href="autocomplete.css" type="text/css" media="screen">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Result Processing System, District Education Department, Makwanpur Nepal</title>
</head>
<?php 
if($do=='enter_rec')
echo "<body onLoad='main.sn.focus();'>";
elseif($do=='enter_school')
echo "<body onload='sc_form.namee.focus();'>";
elseif($do=='enter_year')
echo "<body onload='mero.year.focus();'>";
elseif($do=='enter_subject')
echo "<body onload='sub_form.year.focus();'>";
elseif($do=='set_year_fmpm')
{?>
<body onload="document.getElementById('yrr').focus();">
<?php }
elseif($do=='enter_sch')
echo "<body onload='mero.school.focus();'>";
elseif($do=='dododo')
echo "<body onload='mero.school.focus();'>";
elseif($do=='doitdo')
echo "<body onload='mero.school.focus();'>";
elseif($do=='validate_edit')
echo "<body onload='mero.pass.focus();'>";
elseif($do=="enter_grace" || $do=="edit_grace")
echo "<body onload='sc_form.year.focus();'>";
elseif($do=='default')
{
?>
            <script src="jquery.js" type="text/javascript"></script>
            <script src="jquery.min.js" type="text/javascript"></script>
            <script src="dimensions.js" type="text/javascript"></script>
            <script src="autocomplete.js" type="text/javascript"></script>
			<script type="text/javascript">

			$(function(){
	    	setAutoComplete("searchField", "results", "autocomplete.php?part=");
			});
			
			</script>
<?php
echo "<body>";
}
else
{
?>
            <script src="jquery.js" type="text/javascript"></script>
            <script src="jquery.min.js" type="text/javascript"></script>
            <script src="dimensions.js" type="text/javascript"></script>
            <script src="autocomplete.js" type="text/javascript"></script>
            <script type="text/javascript">

			$(function(){
	    	setAutoComplete("searchField", "results", "autocomplete.php?part=");
			});
			
			</script>
<?php
echo "<body>";
}
?>
<div id="banner">
<a href="first.php"><img src="pics/homee.png" width="30" height="30" hspace="20" vspace="20"/></a>
</div>
<div id="mainbody"> <!-- main body #######################################-->
<!--<div id="menu">

</div>-->
<div id=nav>
<a href="first.php">Home</a><a href="first.php?do=select_marksheet">Marksheet</a><a href="first.php?do=view_ledger">Ledger</a><a href="first.php?do=showind">Individual Entry</a><a href="first.php?do=enter_rec">Enter Record</a><a href="first.php?do=settings">Settings</a><a href="import_first.php">Import Record</a><a href="marksheet_auto1.php">Auto MK Sheet</a>
</div>
<div id="body">
<?php	
		if($do=="enter_rec")
		include("enter_record.php");
		elseif($do=="enter_year")
		include("enter_year.php");
		elseif($do=="enter_year_flush")
		include("enter_year_flush.php");
		elseif($do=="enter_school")
		include("enter_school.php");
		elseif($do=="enter_sch_flush")
		include("enter_school_flush.php");
		elseif($do=="delsuccess")
		include("delsuccess.php");
		elseif($do=="enter_grace")
		include("enter_grace.php");
		elseif($do=="enter_subject")
		include("enter_subject.php");
		elseif($do=="enter_sch")
		include("next.php");
		elseif($do=="enter_sch123")
		include("next123.php");
		elseif($do=="view_mksheet")
		include("select_student.php");
		elseif($do=="view_ledger")
		include("select_ledger.php");
		elseif($do=="edit_record")
		include("edit_record.php");
		elseif($do=="select_marksheet")
		include("select_marksheet.php");
		elseif($do=="showedit")
		include("includes/show_edit_record.php");
		elseif($do=="confirm")
		include("includes/confirm.php");
		elseif($do=="set_fmpm")
		include("set_fmpm.php");
		elseif($do=="settings")
		include("includes/setting.php");
		elseif($do=="dododo")
		include("next1.php");
		elseif($do=="doitdo")
		include("next11.php");
		elseif($do=="showind")
		include("show_indi_enter.php");
		elseif($do=="indient")
		include("indi_enter.php");
		elseif($do=="setting_fmpm")
		include("includes/setting_fmpm.php");
		elseif($do=="detail_fmpm")
		include("includes/detail_fmpm.php");
		elseif($do=="set_year_fmpm")
		include("set_year_fmpm.php");
		elseif($do=="stdDtl")
		include("includes/studentDetail.php");
		elseif($do=="shRes")
		include("includes/showSearchResult.php");
		elseif($do=="report_overall")
		{
		include("report_overall.php");
		echo "<br><a href=report_overall.php><input type=button value='Preview Printable'></a>";
		}
		elseif($do=="validate_edit")
		include("includes/validate_edit.php");
		elseif($do=="default")
		{?>
    
        <div style="float:right; padding:3px;">
        <form action="includes/searchStudent.php" method="post" name="form1">
<p id="auto">
		<label>Search Student: </label>
		<input id="searchField" name="first" type="text" style="width:150px;" />&nbsp;&nbsp;<input type="submit" value="Go" />
	</p>

</form>
</div>
        <br />
        <center><a href="first.php?do=report_overall" style="background-color:brown;text-decoration:none;color:white;">View Overall Report</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</center>
        <br />        <br /><br />
        <br />
        <center>
		<img src="pics/disp.png" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <br />
<br />
<br />
<a href="includes/updateall.php" style="text-decoration:none; color:#990000">Update Database</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br /><br />
<br />

<?php if(isset($_GET['sc']))
			if($_GET['sc']=="yes")
				echo "<font color=green>Database Successfully Updated</font>";
				?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </center>

        <?php
		}
		?>
</div>
<br style="clear:both;" />
</div>           <!-- main body #######################################-->
</body>
</html>