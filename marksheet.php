<?php
include("includes/connect.php");
include("includes/functions.php");
$passpercentage = mysql_result(mysql_query("select * from curriculum"), 0, 1);
$curriculum = mysql_result(mysql_query("select * from curriculum"), 0, 0);
if (!isset($_COOKIE['school']) || !isset($_COOKIE['year']))
    header("location:index.php?do=enter_year");
$year = $_COOKIE['year'];
if (isset($_GET['sn'])) {
    $sn = $_GET['sn'];
    $sturec = mysql_query("SELECT * FROM `main` where `year`=$year AND `sn`='$sn'");
} elseif (isset($_GET['name'])) {
    $name = $_GET['name'];
    $sturec = mysql_query("SELECT * FROM `main` where `year`=$year  AND `name`='$name'");
} elseif (isset($_POST['name'])) {
    $name = $_POST['name'];
    $sturec = mysql_query("SELECT * FROM `main` where `year`=$year AND `name`='$name'");
} elseif (isset($_POST['sn'])) {
    $name = $_POST['name'];
    $sturec = mysql_query("SELECT * FROM `main` where `year`=$year AND `name`='$name'");
} else
    header("location:first.php");


$school = 1;
$subjects = mysql_query("SELECT * from subject where `year`='$year'");
$record = mysql_fetch_assoc($sturec);
$snn = $record['sn'];
$resultG = mysql_fetch_assoc(mysql_query("select * from maingrade where year=".$year." sn='".$snn."'"));
$mar = getmarks($year, $school, $snn);
$res = $record['result'];
$grace = intval($record['grace']);
if (!$grace)
    $grace = 0;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Mark Sheet</title>
    <style type="text/css">
        <!--
        .sabc {
            font-weight: bold;
            font-size: 12pt;
            color: #000000;
            font-family: arial;
            text-decoration: underline;
        }

        tr {
            border: #000000 thin solid;
            border-collapse: collapse;
        }

        .noborder {
            border: none;
        }

        a {
            text-decoration: none;
        }

        a .special {
            font-weight: bold;
            font-size: 14pt;
            color: #000000;
            font-family: verdana;
        }

        a .sabc1 {
            font-weight: bold;
            font-size: 15pt;
            color: #000000;
            font-family: verdana;
            text-decoration: underline;
        }

        .special1 {
            font-weight: bold;
            font-size: 13pt;
            color: #000000;
            font-family: arial;
        }

        .inner_table th, .inner_table td, .inner_table tr {
            border: #999999 thin solid;
            border-collapse: collapse;
        }

        /*
        .inner_table table,tr
        {
        border:none;
        }
        .inner_table td
        {
        border-bottom:none;
        border-top:none;
        border-left:none;
        border-right:#000000 thin solid;
        border-collapse:collapse;
        }
        .inner_table th
        {
        border-bottom:#000000 thin solid;
        border-right:#000000 thin solid;
        }*/
        -->
    </style>
</head>

<body>
<table cellspacing="0" cellpadding="0" class="outer_table"
       style="width:7.27in;height:9in; background-image:url(pics/pachadi1111.png); background-repeat:repeat-y;">
    <tr style="border-bottom:none;">
        <td style="width:7.27in;height:2in; background:url(pics/top_matksheet1111.png); background-position:center; border-bottom:none;"
            onclick="window.print();"></td>
    </tr>
    <tr style="border-top:none; border-bottom:none;">
        <td style="padding-left:10px;padding-top:10px;padding-bottom:10px; font-size:13pt; line-height:2; border-top:none; border-bottom:none;"
            valign="top">
            This is to certify that
            <?php
            if (substr($record['gender'], 0, 1) == 'M') {
                echo "Mr.";
            } elseif (substr($record['gender'], 0, 1) == 'F') {
                echo "Miss";
            } else {
                echo "Mr/Miss. ";
            }
            ?>
            <a href="marksheet.php?sn=<?php if (substr($snn, 0, 1) == 0) echo "0"; else echo ""; ?><?php echo $snn + 1; ?>"><span
                    <?php
                    echo "class=sabc1";
                    ?>>
<?php
echo $record['name'];
?></span></a><br/>
            <?php
            if (substr($record['gender'], 0, 1) == 'M') {
                echo " son ";
            } elseif (substr($record['gender'], 0, 1) == 'F') {
                echo " daughter ";
            } else {
                echo " son/daughter ";
            }
            ?>of Mr <span
                <?php
                echo "class=sabc";
                ?>>
<?php
echo $record['father'];
?>
</span> and Mrs <span
                <?php
                echo "class=sabc";
                ?>> <?php echo $record['mother']; ?></span><br>
            Date of Birth <span class="sabc">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $record['dob']; ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>


            Symbol No <span
                    class="sabc">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $record['sn']; ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br/>
            School <span class="sabc">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo schoolfromid($record['school']); ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            has secured below described marks in the Basic Level Education Completion Examination, Grade 8 of
            year <?php echo $record['year']; ?>
        </td>
    </tr>
    <tr style="border-top:none; border-bottom:none;">
        <td style="height:3.5in; border-top:none; border-bottom:none;">
            <?php
            if ($year < 2066) {
                ?>
                <table cellspacing="0" width="98%" height="96%" class="inner_table" align="center">
                    <tr align="center">
                        <th style="height:0.33in;">Sn</th>
                        <th width="300">Subject</th>
                        <th>Credit Hour</th>
                        <th>Obtained Grade</th>
                        <th>Grade Point</th>
                        <th>Remarks</th>
                    </tr>
                    <?php
                    $data = mysql_fetch_row($subjects);

                    $no = $data[2];
                    for ($i = 3, $sub = 1; $i <= $no + 2; $i++, $sub++) {
                        echo "<TR align=center><td>" . $sub . "</td><td align=left style='height:0.33in;padding-left:10px;'>" . $data[$i] . "</td>";
                        echo "<td>100</td><td>" . $passpercentage . "</td>";
                        $markname = "sub" . $sub;
                        $markname1 = "sub" . $sub . "1";
                        $xxx = ($record[$markname] + $record[$markname1]);
                        echo "<td align=left>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                        if ($xxx < $passpercentage)
                            echo putmarks($xxx, $res, $mar);
                        else
                            echo $xxx;
                        if ($xxx <= $passpercentage)
                            if (!isgrace($mar))
                                echo "&nbsp;&nbsp;&nbsp;*";
                            else
                                echo "&nbsp;";
                        else
                            echo "&nbsp;";
                        echo "</td>";
                        echo "<td style='border-right:none';>";
                        if ($xxx < $passpercentage)
                            if (isgrace($mar))
                                echo "G";
                        echo "</td></tr>";

                    }
                    ?>
                    <tr></tr>
                </table>
                <?php
            } else {
                ?>
                <table cellspacing="0" width="98%" height="90%" class="inner_table" align="center">
                    <tr align="center">
                        <th style="height:0.33in;" rowspan="2">Sn</th>
                        <th width="300" rowspan="2">Subject</th>
                        <th rowspan="2">Credit Hour</th>
                        <th colspan="3">Obtained Grade</th>
                        <th rowspan="2">Grade Point</th>
                        <th rowspan="2">Remarks</th>
                    </tr>
                    <tr>
                        <td>TH</td>
                        <td>PR</td>
                        <td>Total</td>
                    </tr>
                    <?php
                    $fmth = getfmth($year);
                    $fmpr = getfmpr($year);
                    $subs = givsub($year);
                    $no = getsub($year);
                    $school = $record['school'];
                    $mth = getmarkth($year, $school, $snn);
                    $mpr = getmarkpr($year, $school, $snn);
                    $totalCredit = 0;
                    $gradePoint = 0;
                    for ($i = 1; $i <= $no; $i++) {

                        echo "<tr><td>$i</td><td>" . $subs[$i - 1] . "</td><td align=center>";
                        $subjectTotal = $fmth[$i -1];
                        if ($fmpr[$i - 1] != 0){
                            $subjectTotal += $fmpr[$i -1];
                            //echo $fmth[$i - 1];
                        }
                        if($subjectTotal == 100){
                            $subjectCredit = 4;
                        }else{
                            $subjectCredit = 2;
                        }
                        $totalCredit += $subjectCredit;
                        echo $subjectCredit;
                        echo "</td><td>";

                        $grma = 0;
                        $theoryGPA = getGPA($resultG['sub' . $i]);
                        $subjectWeight = $resultG['sub' . $i];
                        echo $theoryGPA[1];
                        echo "</td><td>";
                        if ($mpr[$i - 1] > 0){
                            $practicalGPA = getGPA($resultG['sub'.$i."1"]);
                            echo $practicalGPA[1];
                            $subjectWeight = ($subjectWeight + $resultG['sub'.$i."1"])/2;
                            //echo $mpr[$i - 1];
                        } else
                            echo ' ';
                        echo "</td><td>";
                        $subjectGPA = getGPA($subjectWeight);
                        echo $subjectGPA[1];
                        //echo $mth[$i - 1] + $mpr[$i - 1];
//                        if ($grma > 0)
//                            echo "+" . $grma;
                        echo "</td><td>";
                        $subjectGradePoint = $subjectCredit==2?$subjectGPA[0]/2:$subjectGPA[0];
                        echo $subjectGradePoint;
                        $gradePoint += $subjectGradePoint;
                        echo "</td><td align=center>";

                        echo "</td></tr>";

                    }
                    ?>
                    <tr>
                        <td colspan="2">Total</td>
                        <td>
                            <?php echo $totalCredit;?>
                        </td>
                        <td colspan="3">Grade Point Average</td>
                        <td>
                            <?php echo ($gradePoint/$totalCredit)*4; ?>
                        </td>
                        <td></td>
                    </tr>

                </table>


                <?php
            }
            ?>
        </td>
    </tr>
    <tr style="border-bottom:none; border-top:none;">
        <td style="border-bottom:none; border-top:none;height:1.3in;" valign="top">
            <table class="noborder" align="center" width="98%" style="font-size:13pt; line-height:1.5">
                <tr class="noborder">
                    <td class="noborder">
                        * : Failed<br/>
                        G : Grace
                    </td>
                    <td class="noborder inner_table" width="400">
                        <?php
                        if ($curriculum == 2) {
                            ?>
                            <table style="margin:auto;" cellpadding="5">
                                <tr>
                                    <td colspan=4 align='center'>Grading Scheme</td>
                                </tr>
                                <tr>
                                    <td align='center'>FM</td>
                                    <td align='center'>A</td>
                                    <td align='center'>B</td>
                                    <td align='center'>C</td>
                                </tr>
                                <tr>
                                    <td>800</td>
                                    <td>80%-100%</td>
                                    <td>60%-79%</td>
                                    <td>40%-59%</td>
                                </tr>
                            </table>
                            <?php
                        } else {
                            ?>
<!--                            <table style="margin:auto;" cellpadding="5">-->
<!--                                <tr>-->
<!--                                    <td>A+</td>-->
<!--                                    <td>A+</td>-->
<!--                                    <td>A+</td>-->
<!--                                    <td>A+</td>-->
<!--                                    <td>A+</td>-->
<!--                                    <td>A+</td>-->
<!--                                    <td>A+</td>-->
<!--                                    <td>A+</td>-->
<!--                                    <td>A+</td>-->
<!--                                    <td>A+</td>-->
<!--                                    <td>A+</td>-->
<!--                                    <td>A+</td>-->
<!--                                    <td>A+</td>-->
<!--                                    <td>A+</td>-->
<!--                                    <td>Dist.</td>-->
<!--                                    <td>1st Div</td>-->
<!--                                    <td>2nd Div</td>-->
<!--                                    <td>3rd Div</td>-->
<!--                                </tr>-->
<!--                                <tr>-->
<!--                                    <td>700</td>-->
<!--                                    <td>560</td>-->
<!--                                    <td>420</td>-->
<!--                                    <td>315</td>-->
<!--                                    <td>224</td>-->
<!--                                </tr>-->
<!--                            </table>-->
                            <?php
                        }
                        ?>
                    </td>
                    <td class="noborder">
                        <table class="noborder">
                            <tr class="noborder">
                                <td>
                                    Grade:
                                </td>
                                <td><span class="special1">
        <?php
        echo $resultG['gpa'];

        ?></span></td>
                            </tr>
                            <tr class="noborder">
                                <td>
                                    GPA:
                                </td>
                                <td><span class="special1"><?php echo round($resultG['gradepoint'],2); ?></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table cellspacing="0" width="98%" height="96%" class="inner_table" align="center">
                <caption>Subjects offered By School</caption>
                <tr align="center">
                    <th style="height:0.33in;" rowspan="2">Sn</th>
                    <th width="300" rowspan="2">Subject</th>
                    <th rowspan="2">Full Marks</th>
                    <th rowspan="2">Pass Marks</th>
                    <th colspan="3">Obt Marks</th>
                    <th style="border-right:none;" rowspan="2">Remarks</th>
                </tr>
                <tr>
                    <td>TH</td>
                    <td>PR</td>
                    <td>Total</td>
                </tr>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td></td>
                    <td align=center></td>
                    <td align=center></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td align=center></td>
                </tr>
                <tr>
                    <td><?php echo $i + 1; ?></td>
                    <td></td>
                    <td align=center></td>
                    <td align=center></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td align=center></td>
                </tr>
            </table>

        </td>
    </tr>
    <tr style="border-top:none;">
        <!--style="background:url(pics/bottom.png)-->
        <td height:0.5in; border-top:none;
        "><br/>
        <br/>
        <br/>


        <img src="pics/bottom.png"/>
        Date of Issue:
        </td>
    </tr>
</table>
<?php
?>
</body>
</html>