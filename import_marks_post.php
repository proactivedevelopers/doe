<?php
include 'includes/connect.php';
$year = $_COOKIE['year'];
$school = $_COOKIE['school'];
$import_type = $_POST['import_type'];
setcookie('import_type', $import_type);
$countme = 0;
/** Set default timezone (will throw a notice otherwise) */
date_default_timezone_set('Asia/Kathmandu');

include 'PHPExcel/IOFactory.php';
include 'includes/functions.php';
if (isset($_FILES['file']['name'])) {
    $file_name = $_FILES['file']['name'];
    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
    //Checking the file extension
    if ($ext == "xlsx") {

        $file_name = $_FILES['file']['tmp_name'];
        $inputFileName = $file_name;

        /**********************PHPExcel Script to Read Excel File**********************/

        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName); //Identify the file
            $objReader = PHPExcel_IOFactory::createReader($inputFileType); //Creating the reader
            $objPHPExcel = $objReader->load($inputFileName); //Loading the file
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                . '": ' . $e->getMessage());
        }
        //Table used to display the contents of the file
        echo '<center><table style="width:50%;" border=1>';

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);     //Selecting sheet 0
        $highestRow = $sheet->getHighestRow();     //Getting number of rows
        $highestColumn = $sheet->getHighestColumn();     //Getting number of columns

        //  Loop through each row of the worksheet in turn
        for ($row = 1; $row <= $highestRow; $row++) {

            //  Read a row of data into an array

            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                NULL, TRUE, FALSE);
            // This line works as $sheet->rangeToArray('A1:E1') that is selecting all the cells in that row from cell A to highest column cell


            echo "<tr>";
            $symbol = $rowData[0][0];
            //echoing every cell in the selected row for simplicity. You can save the data in database too.
            if ($import_type == 'practical') {
                $sub1 = $rowData[0][1];
                $sub2 = $rowData[0][2];
                $sub3 = $rowData[0][3];
                $sub4 = $rowData[0][4];
                $sub5 = $rowData[0][5];
                $sub6 = $rowData[0][6];
                $sub7 = $rowData[0][7];
                $sub8 = $rowData[0][8];
                $sub9 = $rowData[0][9];
                if (mysql_query("update main set
                    `sub11` = $sub1,
                    `sub21` = $sub2,
                    `sub31` = $sub3,
                    `sub41` = $sub4,
                    `sub51` = $sub5,
                    `sub61` = $sub6,
                    `sub71` = $sub7,
                    `sub81` = $sub8,
                    `sub91` = $sub9
                    where `year` = '$year' and `school` = '$school' and `sn` = '$symbol'")) {
                    foreach ($rowData[0] as $dataa) {
                        echo "<td>" . $dataa . "</td>";
                    }
                    $countme++;
                }
            } else {
                $sub1 = $rowData[0][1];
                $sub2 = $rowData[0][2];
                $sub3 = $rowData[0][3];
                $sub4 = $rowData[0][4];
                $sub5 = $rowData[0][5];
                $sub6 = $rowData[0][6];
                $sub7 = $rowData[0][7];
                $sub8 = $rowData[0][8];
                $sub9 = $rowData[0][9];
                if (mysql_query("update main set
                    `sub1` = $sub1,
                    `sub2` = $sub2,
                    `sub3` = $sub3,
                    `sub4` = $sub4,
                    `sub5` = $sub5,
                    `sub6` = $sub6,
                    `sub7` = $sub7,
                    `sub8` = $sub8,
                    `sub9` = $sub9
                    where `year` = '$year' and `school` = '$school' and `sn` = '$symbol'
                ")) {
                    foreach ($rowData[0] as $data) {
                        echo "<td>" . $data . "</td>";
                    }
                    $countme++;
                }
            }
        }

        echo '</table></center>';
        echo "<br><center><span style='font-size:100pt'>";
        echo $countme . " Records Imported Successfully";
        echo "<br><a href=import_marks.php>Click Here</a>";
        echo "</span><center>";

    } else {
        echo '<p style="color:red;">Please upload file with xlsx extension only</p>';
    }

}
?>