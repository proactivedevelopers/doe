<?php
include("includes/connect.php");
include("includes/functions.php");

if(!isset($_COOKIE['school']) || !isset($_COOKIE['year']))
header("location:index.php?do=enter_year");
$year=$_COOKIE['year'];

if(isset($_GET['sn']))
{
$sn=$_GET['sn'];
$sturec=mysql_query("SELECT * FROM `main` where `year`=$year AND `sn`='$sn'");
}
elseif(isset($_GET['name']))
{
$name=$_GET['name'];
$sturec=mysql_query("SELECT * FROM `main` where `year`=$year  AND `name`='$name'");
}
elseif(isset($_POST['name']))
{
$name=$_POST['name'];
$sturec=mysql_query("SELECT * FROM `main` where `year`=$year AND `name`='$name'");
}
elseif(isset($_POST['sn']))
{
$name=$_POST['name'];
$sturec=mysql_query("SELECT * FROM `main` where `year`=$year AND `name`='$name'");
}
else
header("location:first.php");



$subjects=mysql_query("SELECT * from subject where `year`='$year'");
$record=mysql_fetch_assoc($sturec);
$snn=$record['sn'];
$mar=getmarks($year,$school,$snn);
$res=$record['result'];
$grace=intval($record['grace']);
if(!$grace)
$grace=0;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Mark Sheet</title>
<style type="text/css">
<!--
.sabc
{
font-weight:bold;
font-size:10pt;
color:#444444;
font-family:arial;
}
tr
{
border:#000000 thin solid;
border-collapse:collapse;
}
.noborder
{
border:none;
}
a
{text-decoration:none;
}
a .special
{
font-weight:bold;
font-size:14pt;
color:#555555;
font-family:verdana;
}
a .sabc1
{
font-weight:bold;
font-size:12pt;
color:#555555;
font-family:verdana;
}
.special1
{
font-weight:bold;
font-size:13pt;
color:#444444;
font-family:arial;
}
.inner_table th,.inner_table td,.inner_table tr
{
border:#999999 thin solid;
border-collapse:collapse;
}

/*
.inner_table table,tr
{
border:none;
}
.inner_table td
{
border-bottom:none;
border-top:none;
border-left:none;
border-right:#000000 thin solid;
border-collapse:collapse;
}
.inner_table th
{
border-bottom:#000000 thin solid;
border-right:#000000 thin solid;
}*/
-->
</style>
</head>

<body>
<table cellspacing="0" cellpadding="0" class="outer_table" style="width:7.27in;height:9in; background-image:url(pics/pachadi1111.png); background-repeat:repeat-y;">
<tr style="border-bottom:none;">
<td style="width:7.27in;height:2in; background:url(pics/top_matksheet1111.png); background-position:center; border-bottom:none;" onclick="window.print();"></td></tr>
<tr style="border-top:none; border-bottom:none;"><td style="padding-left:10px;padding-top:10px;padding-bottom:10px; font-size:13pt; line-height:2; border-top:none; border-bottom:none;" valign="top"><font color=black>
<font color=white>This is to certify that Mr/Miss <a href="marksheet.php?sn=0<?php echo $snn+1 ;?>"><span 
<?php
if(strlen($record['name'])>=20)
echo "class=sabc1";
else
echo "class='special'";
?>>
<?php writespace(10);
$nname=$record['name'];
$length=strlen($nname);
echo $record['name'];
writespace((70-($length+35)));
?></span></a><br />
son / daughter of Mr  <span 
<?php
if(strlen($record['father'])>=20)
echo "class=sabc";
else
echo "class='special1'";
?>
>&nbsp;&nbsp;<?php 

if(strlen($record['father'])<20)
{
$spcs=20-strlen($record['father']);
echo $record['father'];
writespace(floor($spcs/2)*2);
writespace(floor($spcs/2)*2);
}
else 
echo $record['father'];

?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>and Mrs <span 
<?php
if(strlen($record['mother'])>=16)
echo "class=sabc";
else
echo "class='special1'";
?>
>&nbsp;<?php echo $record['mother'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br>
Date of Birth <span class="special1">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $record['dob'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>


Symbol No <span class=special1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $record['sn'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br />

School <span class=special1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo schoolfromid($record['school']);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br />
has secured below described marks in the Basic Level Final Examination(Grade 8) of year <?php echo $record['year'];?></font>

</font>
</td></tr>
<tr style="border-top:none; border-bottom:none;">
<td style="height:3.5in; border-top:none; border-bottom:none;">
<?php 
if($year<2066)
{
?>
	<table cellspacing="0" width="98%" height="96%" class="inner_table" align="center">
    <tr align="center"><th style="height:0.33in;">Sn</th><th width="300">Subject</th><th>Full Marks</th><th>Pass Marks</th><th>Obt Marks</th>
Remarks</tr>
	<?php
	$data=mysql_fetch_row($subjects);
	
	$no=$data[2];
	for($i=3,$sub=1;$i<=$no+2;$i++,$sub++)
	{
echo "<TR align=center><td>".$sub."</td><td align=left style='height:0.33in;padding-left:10px;'>".$data[$i]."</td>";
echo "<td>100</td><td>32</td>";
$markname="sub".$sub;
$markname1="sub".$sub."1";
$xxx=($record[$markname]+$record[$markname1]);
echo "<td align=left>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
if($xxx<32)
echo putmarks($xxx,$res,$mar);
else
echo $xxx;
if($xxx<=32)
	if(!isgrace($mar))
	echo "&nbsp;&nbsp;&nbsp;*";
	else 
	echo "&nbsp;";
else
echo "&nbsp;";
echo "</td>";
echo "<td style='border-right:none';>";
if($xxx<32)
	if(isgrace($mar))
		echo "G";
echo "</td></tr>";

}
?>
<tr></tr>
</table>
<?php
}
else
{
?>
	<table cellspacing="0" width="98%" height="96%" class="inner_table" align="center">
    <tr align="center"><th style="height:0.33in;" rowspan="2">Sn</th><th width="300" rowspan="2">Subject</th><th rowspan="2">Full Marks</th><th rowspan="2">Pass Marks</th><th colspan="3">Obt Marks</th><th style="border-right:none;" rowspan="2">Remarks</th></tr>
    <tr><td>TH</td><td>PR</td><td>Total</td></tr>
<?php
$fmth=getfmth($year);
$fmpr=getfmpr($year);
$subs=givsub($year);
$no=getsub($year);
$school=$record['school'];
$mth=getmarkth($year,$school,$snn);
$mpr=getmarkpr($year,$school,$snn);
for($i=1;$i<=$no;$i++)
{
echo "<tr><td>$i</td><td>".$subs[$i-1]."</td><td align=center>";
if($fmpr[$i-1]==0)
echo $fmth[$i-1];
else
echo $fmth[$i-1]." | ".$fmpr[$i-1];
echo "</td><td align=center>";
if($fmpr[$i-1]==0)
echo $pmth=($fmth[$i-1]*32)/100;
else
{
echo $pmth=ceil(($fmth[$i-1]*32)/100);
echo " | ";
echo $pmpr=floor(($fmpr[$i-1]*32)/100);
}
echo "</td><td>";
$grma=0;
if($res=="Passed")
	if($grace>0)
		if($mth[$i-1]<$pmth)
		{
		echo $mth[$i-1];
		$grma=$pmth-$mth[$i-1];
		}
		else
		echo $mth[$i-1];
	else
	echo $mth[$i-1];
else
echo $mth[$i-1];
echo "</td><td>";
if($mpr[$i-1]>0)
echo $mpr[$i-1];
else
echo ' ';
echo "</td><td>";
echo $mth[$i-1]+$mpr[$i-1];
if($grma>0)
echo "+".$grma;
echo "</td><td align=center>";
if($grma>0)
echo "G";
if($res=="Failed")
	if($mth[$i-1]<$pmth)
		echo "*";
echo "</td></tr>";

}

    
    
?>    

</table>



<?php
}
?>
</td>
</tr>
<tr style="border-bottom:none; border-top:none;">
<td style="border-bottom:none; border-top:none;height:1.3in;" valign="top">
<br>
<table class="noborder" align="center" width="98%" style="font-size:13pt; line-height:1.5">
<tr class="noborder">
<td class="noborder"><font color=black>
* : Failed<br />
G : Grace</td>
<td class="noborder" width="400"></td>
<td class="noborder">
Total: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="special1">
<?php 
echo $record['total'];
if($grace>0)
echo "+".$grace;
?>&nbsp;&nbsp;&nbsp;</span><br />
<!--Percentage: &nbsp;&nbsp;<span class="special1"><?php /*echo $record['per'];*/?>&nbsp;&nbsp;&nbsp;&nbsp;</span><br />-->
Result: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="special1"><?php echo $record['result'];?></span>
</td>
</tr>
<tr class="noborder">
<td class="noborder">
<br>
<br>
<br><br>
</td>
</tr>
</table>

</td>
</tr>
<tr style="border-top:none;">
<td style="background:url(pics/bottom111.png); height:0.5in; border-top:none;">&nbsp;<br><br><br></td>
</tr>
</table>
<?php
/*print_r($mar);*/
?>
</body>
</html>

